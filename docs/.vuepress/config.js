module.exports = {
  title: "Harry Potter",
  description: "The description of the site.",
  head: [["link", { rel: "icon", href: `/logo.png` }]],
  base: "/",
  dest: "./dist",

  themeConfig: {
    search: false,
    nav: [
      { text: "Home", link: "/" },
      { text: "About", link: "/about/" },
      { text: "Projects", link: "/projects/" },
      { text: "Guide", link: "/guide/" },
      { text: "GitHub", link: "https://github.com/mtobeiyf/vuepress-homepage" },
    ],
    sidebar: {
      "/guide/": genSidebarConfig("Guide"),
    },
    lastUpdated: "Last Updated",
  },
};

function genSidebarConfig(title) {
  return [
    {
      title,
      collapsable: false,
      children: ["", "getting-started", "customize", "advanced"],
    },
  ];
}
